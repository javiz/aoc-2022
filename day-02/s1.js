let sum = 0;
const m = {A: {X: 3, Y: 4, Z: 8}, B: {X: 1, Y: 5, Z: 9}, C: {X: 2, Y: 6, Z: 7}};
require('../read')((line) => {
  const [a, b] = line.split(" ");
  sum += m[a][b];
}, () => console.log('sum', sum));

