let sum = 0;
const m = {A: {X: 4, Y: 8, Z: 3}, B: {X: 1, Y: 5, Z: 9}, C: {X: 7, Y: 2, Z: 6}};
require('../read')((line) => {
  const [a, b] = line.split(" ");
  sum += m[a][b];
}, () => console.log('sum', sum));

