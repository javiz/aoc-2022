module.exports = (cb0, cb1) => require('readline')
  .createInterface({input: require('fs').createReadStream('i.txt')})
  .on('line', cb0).on('close', cb1);

