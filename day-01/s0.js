let max = sum = 0;
require('../read')((line) => {
  if (line === "") {
    if (sum > max) max = sum;
    sum = 0;
  } else sum += parseInt(line);
}, () => console.log('max', max));
