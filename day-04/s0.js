let sum = 0;
require('../read')((line) => {
  const [a, b] = line.split(",");
  const [a0, a1] = a.split('-').map((a) => parseInt(a));
  const [b0, b1] = b.split('-').map((a) => parseInt(a));
  sum += ((a0 <= b0 && a1 >= b1) || (b0 <= a0 && b1 >= a1)) ? 1 : 0;
}, () => console.log('sum', sum));
