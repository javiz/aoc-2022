let stacks = [
  ["D", "H", "N", "Q", "T", "W", "V", "B"],
  ["D", "W", "B"],
  ["T", "S", "Q", "W", "J", "C"],
  ["F", "J", "R", "N", "Z", "T", "P"],
  ["G", "P", "V", "J", "M", "S", "T"],
  ["B", "W", "F", "T", "N"],
  ["B", "L", "D", "Q", "F", "H", "V", "N"],
  ["H", "P", "F", "R"],
  ["Z", "S", "M", "B", "L", "N", "P", "H"],
]

require('../read')((line) => {
  const [n, from, to] = /move ([0-9]*) from ([0-9]*) to ([0-9]*)/.exec(line).slice(1,4).map((a) => parseInt(a));
  Array.apply(null, {length: n}).forEach(() => stacks[to-1].push(stacks[from-1].pop()))
}, () => console.log('last', stacks.map((el) => el[el.length-1])));
