const d = {};
let s = [];
let t = 0;
require('../read')((l) => {
  const [w0, w1, w2] = l.split(" ");
  const n = parseInt(w0);
  if (!isNaN(n)) {
    t += n;
    s.forEach((_, i) => {
      path = s.slice(0,i+1).join("/");
      d[path] = d[path] ? d[path] + n : n;
    })
  }
  s = w1 === "cd" ? (w2 === "/" ? [] : w2 === ".." ? s.slice(0,s.length-1) : [...s, w2]) : s;
}, () => {
  const sorted = Object.values(d).sort((a,b) => b-a);
  console.log('solution', sorted[sorted.findIndex((el) => el < 30000000 - (70000000 - t))-1]);
});
