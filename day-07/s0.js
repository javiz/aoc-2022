const d = {};
let s = [];
require('../read')((l) => {
  const [w0, w1, w2] = l.split(" ");
  const n = parseInt(w0);
  if (!isNaN(n)) {
    s.forEach((_, i) => {
      path = s.slice(0,i+1).join("/");
      d[path] = d[path] ? d[path] + n : n;
    })
  }
  s = w1 === "cd" ? (w2 === "/" ? [] : w2 === ".." ? s.slice(0,s.length-1) : [...s, w2]) : s;
}, () => {
  const sorted = Object.values(d).sort((a,b) => b-a);
  console.log("solution", sorted .filter((el) => el <= 100000).reduce((a,b) => a + b, 0));
});
