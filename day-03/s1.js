let it = sum = 0;
let acc = [];
const convert = (s) => s.toLowerCase().charCodeAt(0) - 96 + (s.toLowerCase() === s ? 0 : 26);
const common = (a, b, c) => a.split("").find((el) => b.indexOf(el) >= 0 && c.indexOf(el) >= 0);
require('../read')((line) => {
  it = (it + 1)%3;
  acc.push(line);
  if (it !== 0) return;
  sum += convert(common(...acc));
  acc = [];
}, () => console.log('sum', sum));
