let sum = 0;
const convert = (s) => s.toLowerCase().charCodeAt(0) - 96 + (s.toLowerCase() === s ? 0 : 26);
const common = (a, b) => a.split("").find((el) => b.indexOf(el) >= 0);
require('../read')((line) => {
  sum += convert(common(line.slice(0, line.length/2),line.slice(line.length/2)));
}, () => console.log('sum', sum));
